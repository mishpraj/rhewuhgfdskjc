#PULLING GITPOD/OPENVSCODE-SERVER image

FROM gitpod/openvscode-server:latest
USER root
WORKDIR /home/openvscode-server
RUN sudo mkdir /home/codestation
RUN sudo chmod 777 /home/codestation

#GENERAL PURPOSE PACKAGES
RUN apt-get update -y && apt-get upgrade -y && apt-get install -y sudo && apt-get install -y python3-pip && pip3 install --upgrade pip

RUN apt-get update
RUN sudo apt-get install -y gnupg curl wget htop sudo git gpg git-lfs mercurial cmake bzip2 dpkg flatpak snapd software-properties-common build-essential libgl1 zip unzip tar vim nano neovim emacs apt-transport-https software-properties-common --fix-missing

#PYTHON PACKSAGES
RUN apt-get update
RUN sudo pip3 install numpy pandas scikit-learn scikit-image requests keras torch tensorflow opencv-python scipy seaborn lightgbm matplotlib pillow pytest nltk urllib3 matplotlib-inline mpmath httpx httpcore certifi cryptography async-lru arrow ipykernel ipython jedi jupyter-events jupyter-lsp jupyter-server-mathjax jupyter_client jupyter_core jupyter_server jupyterlab jupyter_server_terminals jupyterlab jupyterlab_git jupyterlab_pygments jupyterlab_server MarkupSafe kiwisolver beautifulsoup4 PyYAML                             

#NVTOP
RUN apt-get update
RUN add-apt-repository ppa:flexiondotorg/nvtop
RUN apt-get upgrade -y
RUN sudo apt-get install -y nvtop

#EXA 
RUN apt-get update
RUN sudo apt install -y exa

#HTTPie
RUN apt-get update
RUN curl -SsL https://packages.httpie.io/deb/KEY.gpg | sudo gpg --dearmor -o /usr/share/keyrings/httpie.gpg
RUN echo "deb [arch=amd64 signed-by=/usr/share/keyrings/httpie.gpg] https://packages.httpie.io/deb ./" | sudo tee /etc/apt/sources.list.d/httpie.list > /dev/null
RUN sudo apt update
RUN sudo apt install -y httpie

#POWERSHELL
RUN apt-get update
RUN sudo apt-get install -y apt-transport-https software-properties-common --fix-missing
RUN wget -q "https://packages.microsoft.com/config/ubuntu/$(lsb_release -rs)/packages-microsoft-prod.deb"
RUN sudo dpkg -i packages-microsoft-prod.deb
RUN apt-get update
RUN sudo apt-get install -y powershell

#FISH SHELL
RUN apt-get update
RUN apt-add-repository -y ppa:fish-shell/release-3
RUN apt-get update && sudo apt-get upgrade
RUN sudo apt-get install -y fish

#KORNNSHELL
RUN apt-get update 
RUN sudo apt-get install -y ksh

#PHP 
RUN apt-get update
RUN sudo apt install -y php libapache2-mod-php
RUN sudo apt install -y php-cli
RUN sudo apt install -y php-cgi
RUN sudo apt install -y php-mysql
RUN sudo apt install -y php-pgsql

#DOCKER 
RUN apt-get update
RUN apt-get install -y ca-certificates curl
RUN sudo install -m 0755 -d /etc/apt/keyrings
RUN sudo curl -fsSL https://download.docker.com/linux/ubuntu/gpg -o /etc/apt/keyrings/docker.asc
RUN sudo chmod a+r /etc/apt/keyrings/docker.asc
RUN sudo echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.asc] https://download.docker.com/linux/ubuntu \
  $(. /etc/os-release && echo "$VERSION_CODENAME") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
RUN apt-get update
RUN sudo apt-get install -y -f docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin 

#GO 
RUN apt-get update
RUN curl -OL https://golang.org/dl/go1.16.7.linux-amd64.tar.gz
RUN sudo tar -C /usr/local -xvf go1.16.7.linux-amd64.tar.gz
RUN export PATH=$PATH:/usr/local/go/bin 
RUN apt-get update
RUN sudo apt install -y golang-go

#GITHUB CLI
RUN apt-get update
RUN curl -fsSL https://cli.github.com/packages/githubcli-archive-keyring.gpg | gpg --dearmor -o /usr/share/keyrings/githubcli-archive-keyring.gpg;
RUN echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/githubcli-archive-keyring.gpg] https://cli.github.com/packages stable main" | tee /etc/apt/sources.list.d/github-cli.list > /dev/null;
RUN apt-get update && apt-get install -y gh;

#NODE JS
RUN apt-get update
RUN curl -sL https://deb.nodesource.com/setup_20.x  | bash - && \
    apt-get install -y nodejs && \
    npm install -g configurable-http-proxy

#BUN 
RUN apt-get update
RUN npm install -g bun

#C & CPP
RUN apt-get update
RUN apt-get install -y g++
RUN apt-get install -y gcc

#RUST
RUN apt-get update
RUN sudo curl https://sh.rustup.rs -sSf | bash -s -- -y --no-modify-path
RUN echo 'source $HOME/.cargo/env' >> $HOME/.bashrc

#RUBY
RUN apt-get update
RUN apt-get install -y ruby-full

#BRAINFUCK-SHELL NOT SUPPORTED RN-$(13-05-2024/V1.01/P<M> ---AUTH 200)
# RUN apt-get update
# RUN sudo snap install brainfuck-shell

#JAVA 
RUN apt-get update
RUN apt-get install -y default-jre
RUN apt-get install -y default-jdk

#JENKINS
RUN sudo wget -O /usr/share/keyrings/jenkins-keyring.asc \
  https://pkg.jenkins.io/debian-stable/jenkins.io-2023.key
RUN echo "deb [signed-by=/usr/share/keyrings/jenkins-keyring.asc]" \
  https://pkg.jenkins.io/debian-stable binary/ | sudo tee \
  /etc/apt/sources.list.d/jenkins.list > /dev/null
RUN apt-get update
RUN apt-get install -y jenkins

#ZSH SHELL 
RUN apt-get update 
RUN sudo apt-get install -y zsh

#ZIG 
RUN sudo wget "https://ziglang.org/download/0.12.0/zig-linux-x86_64-0.12.0.tar.xz"
RUN sudo tar xf zig-linux-x86_64-0.12.0.tar.xz
RUN echo 'export PATH="$HOME/zig-linux-x86_64-0.12.0:$PATH"' >> ~/.bashrc

#FORTRAN
RUN apt-get update
RUN sudo apt-get install -y manpages-dev
RUN sudo apt-get install -y gfortran

#LISP
RUN apt-get update
RUN sudo apt-get -y install sbcl

#COBOL
RUN apt-get update
RUN sudo apt-get install -y gnucobol

#SCALA
RUN apt-get update
RUN sudo apt-get install -y scala

#JULIA 
RUN apt-get update
RUN wget https://julialang-s3.julialang.org/bin/linux/x64/1.8/julia-1.8.1-linux-x86_64.tar.gz
RUN sudo tar zxvf julia-1.8.1-linux-x86_64.tar.gz
RUN sudo mv julia-1.8.1/ /opt/
RUN sudo ln -s /opt/julia-1.6.4/bin/julia /usr/local/bin/julia
RUN export export PATH=$PATH:/usr/local/julia/bin

#HASKELL 
RUN apt-get update
RUN sudo apt install -y libicu-dev libtinfo-dev libgmp-dev libc6-dev libffi-dev git g++ gcc make xz-utils zlib1g-dev gnupg
RUN sudo apt install -y haskell-platform

#DOTNET C(SHARP)/C#  
RUN apt-get update
RUN wget -q https://packages.microsoft.com/config/ubuntu/22.04/packages-microsoft-prod.deb -O packages-microsoft-prod.deb
RUN sudo dpkg -i packages-microsoft-prod.deb
RUN sudo add-apt-repository universe
RUN apt-get update
RUN sudo apt-get install -y apt-transport-https
RUN apt-get update
RUN sudo apt-get install -y dotnet-sdk-6.0

#R
RUN apt-get update
RUN sudo apt-key adv --keyserver keyserver.ubuntu.com --recv-keys E298A3A825C0D65DFD57CBB651716619E084DAB9
RUN sudo add-apt-repository "deb https://cloud.r-project.org/bin/linux/ubuntu $(lsb_release -cs)-cran40/"
RUN apt-get update
RUN sudo apt install -y r-base

#PERL 
RUN apt-get update
RUN apt-get install -y perl

#LUA 
RUN apt-get update
RUN sudo apt install -y lua5.4 liblua5.4-dev

#PROLOG
RUN sudo add-apt-repository ppa:swi-prolog/stable
RUN sudo apt-get update
RUN sudo apt-get install -y swi-prolog

#DART 
RUN sudo apt-get update && sudo apt-get install apt-transport-https
RUN wget -qO- https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo gpg  --dearmor -o /usr/share/keyrings/dart.gpg
RUN echo 'deb [signed-by=/usr/share/keyrings/dart.gpg arch=amd64] https://storage.googleapis.com/download.dartlang.org/linux/debian stable main' | sudo tee /etc/apt/sources.list.d/dart_stable.list
RUN sudo apt-get update && sudo apt-get install dart

#OBJECTIVE C
RUN apt-get update
RUN sudo apt-get -y install gobjc gnustep gnustep-devel


#ERLANG
RUN apt-get update
RUN apt-get install -y erlang

#REDIS 
RUN curl -fsSL https://packages.redis.io/gpg | sudo gpg --dearmor -o /usr/share/keyrings/redis-archive-keyring.gpg
RUN echo "deb [signed-by=/usr/share/keyrings/redis-archive-keyring.gpg] https://packages.redis.io/deb $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/redis.list
RUN sudo apt-get update
RUN sudo apt-get -y install redis

#ASSEMBLY LANGUAGE
RUN apt-get update 
RUN sudo apt-get install -y as31 nasm  

#LLVM
RUN apt-get update
RUN sudo apt-get install -y llvm

#FFMPEG
RUN apt-get update 
RUN sudo apt install -y ffmpeg

#USER-SETUP
RUN apt-get update
RUN id -u openvscode-server &>/dev/null || (adduser --disabled-password --gecos '' openvscode-server && adduser openvscode-server sudo)
RUN echo 'openvscode-server ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN chown root:root /usr/bin/sudo && chmod 4755 /usr/bin/sudo
RUN sudo chmod 777 /bin/*
RUN sudo chmod 777 /usr/*
RUN sudo chmod 777 /home/*
RUN sudo chmod 777 /var/*
RUN sudo rm -f /var/lib/apt/lists/lock
RUN chmod -R a=rx,u+ws /usr/bin/sudo

USER root

#@ 7860 PORT EXPOSE
EXPOSE 7860
 
#RUNNING SERVER
ENTRYPOINT [ "/bin/sh", "-c", "exec ${OPENVSCODE_SERVER_ROOT}/bin/openvscode-server --host 0.0.0.0 --port 7860 --without-connection-token --default-folder \"/home/codestation\" --default-workspace \"/home/codestation\" \"${@}\"", "--" ]